#pragma once

#include <Wt/WApplication.h>
#include <Wt/WBootstrapTheme.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WServer.h>

#include <Wt/Auth/AuthWidget.h>
#include <Wt/Auth/PasswordService.h>
#include "Session.hpp"

namespace Lancelot
{
using namespace Wt::Core;
class LancelotWidget;

class LancelotApp : public Wt::WApplication
{
public:
    explicit LancelotApp(const Wt::WEnvironment& env);

    static std::unique_ptr<Wt::WApplication> createApp(const Wt::WEnvironment& env);
    static Session& session();

private:
    void handleAuthEvent();
    void handleInternalPathChangedEvent(const std::string& internalPath);

    void changeTheme();
    void addAuthWidget();
    void addLancelotWidget();

    Session m_session;
    observing_ptr<Wt::WStackedWidget> m_mainStack;
    observing_ptr<Wt::WContainerWidget> m_externalAuthContainer;
    observing_ptr<LancelotWidget> m_appWidget;

    // trust me, I'm an engineer
    static inline Session* g_session = nullptr;

};

}
