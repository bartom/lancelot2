#include "ArmorWidget.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"

namespace Lancelot
{
using namespace Wt;

ArmorWidget::ArmorWidget(Wt::Dbo::ptr<Armor> armor):
    GenericItemWidget(std::move(armor))
{
    addDamageReductionAndArmorClassItems();

    m_menu->insertItem(3, "Opis")->clicked().connect(
            this, &ArmorWidget::showArmorDesctiption);

    addArmorWearingItem();
}

void ArmorWidget::showArmorDesctiption()
{
    Wt::WMessageBox::show(m_item->name(),
                          createDescriptionContent(),
                          Wt::StandardButton::Ok);
}

Wt::WString ArmorWidget::createDescriptionContent()
{
    auto content = WString("<p>{1}</p>").arg(m_item->description());

    Dbo::Transaction t(LancelotApp::session());
    if(auto change = m_item->statusChange; change)
    {
        content += WString("<br/>");
        content += WString("<p>Dodatkowy efekt: {1}</p>").arg(change->name());
        if(not change->params.empty())
        {
            content += WString("<p>Zmiany w parametrach:</p>");
            for (auto it = change->params.begin(); it != change->params.end(); ++it)
            {
                const auto[param, value] = (*it)->typeToValue;
                content += WString("<p>{1}: {2}</p>").arg(toString(param)).arg(value);
            }
        }
    }

    return content;
}

void ArmorWidget::addDamageReductionAndArmorClassItems()
{
    auto damageItem = m_menu->insertItem(0, WString("Redukcja obrażeń: {1}")
        .arg(m_item->damageReduction()));
    damageItem->setDisabled(true);

    auto aClassItem = m_menu->insertItem(1, WString("Klasa pancerza: {1}")
        .arg(toString(m_item->aClass())));
    aClassItem->setDisabled(true);
}

void ArmorWidget::addArmorWearingItem()
{
    auto label = m_item->isEquipped() ? "Zdejmij" : "Załóż";

    m_menu->insertItem(4, label)->clicked().connect([&](){
        m_equiped.emit(m_item);
    });

}

}
