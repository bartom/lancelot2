#pragma once

#include <Wt/WDialog.h>
#include <Wt/WSpinBox.h>
#include "PouchDelta.hpp"

namespace Lancelot
{
using namespace Wt::Core;

class PouchChangeDialog: public Wt::WDialog
{
public:
    PouchChangeDialog();

    Wt::Signal<PouchDelta> pouchChanged;

private:
    bool validateAll() const;
    PouchDelta fetchValuesFromWidgets() const;

    observing_ptr<Wt::WSpinBox> m_platBox;
    observing_ptr<Wt::WSpinBox> m_goldBox;
    observing_ptr<Wt::WSpinBox> m_silverBox;
    observing_ptr<Wt::WSpinBox> m_bronzeBox;


};


}



