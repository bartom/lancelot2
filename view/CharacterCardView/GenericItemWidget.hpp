#pragma once

#include <Wt/WPushButton.h>
#include <Wt/Signals/signals.hpp>
#include "Session.hpp"
#include "LancelotApp.hpp"
#include <Wt/WPopupMenu.h>
#include <cmath>

namespace Lancelot
{
class Item;
class Weapon;
class Armor;

template <typename ItemClass>
class GenericItemWidget : public Wt::WPushButton
{
public:
    explicit GenericItemWidget(Wt::Dbo::ptr<ItemClass> item):
        m_item(std::move(item)),
        m_menu(nullptr)
    {
        setText(m_item->name());

        createMenu();
    }

protected:
    Wt::Dbo::ptr<ItemClass> m_item;
    Wt::WPopupMenu* m_menu;

    void removeItem()
    {
        Wt::Dbo::Transaction t(LancelotApp::session());
        m_item.remove();
        removeFromParent();
    }

private:
    void createMenu()
    {
        auto menu = std::make_unique<Wt::WPopupMenu>();
        m_menu = menu.get();

        if constexpr (std::is_same_v<ItemClass, Weapon> or
                      std::is_same_v<ItemClass, Armor>)
        {
            m_menu->addSeparator();
        }

        if constexpr (std::is_same_v<ItemClass, Item>)
        {
            addDescription();
        }

        m_menu->addSeparator();

        addInfo();

        m_menu->addSeparator();

        m_menu->addItem("Usuń")->clicked()
            .connect(this, &GenericItemWidget::removeSelf);

        setMenu(std::move(menu));
    }

    void addDescription()
    {
        m_menu->addItem("Opis")->clicked().connect([&](){
            Wt::WMessageBox::show(m_item->name(), m_item->description(),
                                  Wt::StandardButton::Ok);
        });
    }

    void addInfo()
    {
        auto amount = m_menu->addItem(Wt::WString("Ilość: {1}")
                                            .arg(m_item->amount()));
        amount->setDisabled(true);

        auto weightVal = std::round(m_item->weight()*100);
        auto weight = m_menu->addItem(Wt::WString("Waga: {1}")
                                            .arg(weightVal/100));
        weight->setDisabled(true);
    }

    void removeSelf()
    {
        auto result = Wt::WMessageBox::show("Potwierdzenie",
                "<p>Na pewno chcesz usunąć ten przedmiot?</p>",
                Wt::StandardButton::Yes | Wt::StandardButton::No);

        if (result == Wt::StandardButton::Yes)
        {
            removeItem();
        }
    }
};

}
