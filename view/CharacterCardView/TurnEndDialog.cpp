#include "TurnEndDialog.hpp"
#include "Utility.hpp"
#include <Wt/WPushButton.h>
#include <Wt/WLabel.h>

namespace Lancelot
{
using namespace Wt;

TurnEndDialog::TurnEndDialog():
    WDialog("Koniec tury")
{
    rejectWhenEscapePressed();
    addStyleClass("modal-diff");
    contents()->addStyleClass("modal-body-diff");

    m_healthBox = Utility::addFormToContainerWidget<WSpinBox>("Utracone zdrowie:", contents());
    m_healthBox->setMargin(5, Side::Bottom);
    m_manaBox = Utility::addFormToContainerWidget<WSpinBox>("Utracona mana:", contents());
    m_manaBox->setMargin(5, Side::Bottom);
    m_staminaBox = Utility::addFormToContainerWidget<WSpinBox>("Utracona kondycja:", contents());
    m_staminaBox->setMargin(5, Side::Bottom);

    auto cancel = footer()->addWidget(std::make_unique<WPushButton>("Anuluj"));
    cancel->clicked().connect(this, &TurnEndDialog::reject);

    auto ok = footer()->addWidget(std::make_unique<WPushButton>("OK"));
    ok->clicked().connect(this, &TurnEndDialog::endTurn);
}

void TurnEndDialog::endTurn()
{
    turnEnded.emit({m_healthBox->value(), m_manaBox->value(), m_staminaBox->value()});
    accept();
}


}
