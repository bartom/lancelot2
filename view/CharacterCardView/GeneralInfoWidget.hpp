#pragma once

#include <Wt/WPanel.h>
#include <Wt/Dbo/ptr.h>
#include <Wt/WLineEdit.h>
#include <Wt/WComboBox.h>
#include <Wt/WSpinBox.h>
#include "CommonEnums.hpp"

namespace Lancelot
{
class Character;

class GeneralInfoWidget : public Wt::WPanel
{
public:
    explicit GeneralInfoWidget(Wt::Dbo::ptr<Character> character);
    void update();

    const Wt::WString& getName() const
    {
        return m_name->validate() == Wt::ValidationState::Valid ?
        m_name->text() :
        INCORRECT_NAME_INDICATOR;
    }

    RaceEnum getRace() const
    { return RaceEnum(m_race->currentIndex()); }

    TechClassEnum getTechClass() const
    { return TechClassEnum(m_techClass->currentIndex()); }

    MageClassEnum getMageClass() const
    { return MageClassEnum(m_mageClass->currentIndex()); }

    Deity getDeity() const
    { return Deity{m_deity->currentIndex()}; }

    int getKarma() const
    { return m_karma->value(); }

    int getLevel() const
    { return m_level->value(); }

    int getExp() const
    { return m_exp->value(); }

private:
    inline static const Wt::WString INCORRECT_NAME_INDICATOR = "?";

    void addForms(bool isCharacterSet);
    void updateFromExistingCharacter();
    void updateForNewCharacter();
    void validateName();

    Wt::Dbo::ptr<Character> m_character;

    Wt::WLineEdit* m_name;
    Wt::WText* m_nameIndicator;
    Wt::WComboBox* m_race;
    Wt::WComboBox* m_techClass;
    Wt::WComboBox* m_mageClass;
    Wt::WComboBox* m_deity;
    Wt::WSpinBox* m_karma;
    Wt::WSpinBox* m_level;
    Wt::WSpinBox* m_exp;

};

}
