#pragma once

#include <Wt/WPanel.h>
#include <optional>
#include "ParameterPack.hpp"
#include "Parameter.hpp"

namespace Lancelot
{

class ParametersWidget : public Wt::WPanel
{
public:
    explicit ParametersWidget(std::optional<Parameters> params);
    void update();
    void update(const Parameters& params);

    Parameters getParamsFromWidgetValues() const;

private:
    void addForms();

    std::optional<Parameters> m_params;

    Wt::WSpinBox* m_health;
    Wt::WSpinBox* m_mana;
    Wt::WSpinBox* m_stamina;

    Wt::WSpinBox* m_strength;
    Wt::WSpinBox* m_agility;
    Wt::WSpinBox* m_intelligence;
    Wt::WSpinBox* m_charisma;

    Wt::WSpinBox* m_techAbility;
    Wt::WSpinBox* m_mageAbility;

};

}
