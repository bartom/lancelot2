#include "StatusChangeForm.hpp"
#include "Armor.hpp"
#include "ParameterModel.hpp"
#include "Jewellery.hpp"
#include "Potion.hpp"
#include "LancelotApp.hpp"
#include <Wt/WHBoxLayout.h>
#include <Wt/WComboBox.h>
#include <Wt/WSpinBox.h>
#include <Wt/WCheckBox.h>
#include <Wt/WTextArea.h>
#include <Wt/WRegExpValidator.h>
#include <Wt/WPushButton.h>
#include <Wt/WLabel.h>

namespace Lancelot
{
using namespace Wt;

ParameterChangeForm::ParameterChangeForm()
{
    auto layout = setLayout(std::make_unique<WHBoxLayout>());
    m_params = layout->addWidget(std::make_unique<WComboBox>());

    for(auto param : {Parameter::Health, Parameter::Mana, Parameter::Stamina,
                      Parameter::Strength, Parameter::Agility,
                      Parameter::Intelligence, Parameter::Charisma})
    {
        m_params->addItem(toString(param));
    }

    m_value = layout->addWidget(std::make_unique<WSpinBox>());
    m_value->setMinimum(-30);
    m_value->setMaximum(30);
}

ParameterValue ParameterChangeForm::getChosenValue() const
{
    return ParameterValue{static_cast<Parameter>(m_params->currentIndex()), m_value->value()};
}

bool ParameterChangeForm::validate() const
{
    return m_value->value() != 0 and m_value->validate() == ValidationState::Valid;
}

/////////////////////////////////////////////////////////////////////////////////////

StatusChangeForm::StatusChangeForm(bool isFinite, NameEditor nameEditor)
{
    switch (nameEditor)
    {
        case NameEditor::Checkbox:
        {
            auto showBox = addWidget(std::make_unique<WCheckBox>("<b>Pokaż nazwę i opis statusu</b>"));
            showBox->setTextFormat(TextFormat::XHTML);
            showBox->checked().connect([this](){ m_name->show(); m_description->show(); });
            showBox->unChecked().connect([this](){ m_name->hide(); m_description->hide(); });
            showBox->setMargin(4, Side::Top);
            showBox->setMargin(7, Side::Bottom);
            addWidget(std::make_unique<WBreak>());
            [[fallthrough]];
        }
        case NameEditor::NotExisting:
        case NameEditor::Existing:
        {
            m_name = Utility::addFormToContainerWidget<WLineEdit>("Nazwa zmiany statusu:", this);
            m_name->setMaxLength(20);
            m_name->setValidator(std::make_unique<WRegExpValidator>(Utility::NAME_REGEX_NONUMBER));

            m_description = Utility::addFormToContainerWidget<WTextArea>("Opis zmiany statusu:", this);
            m_description->resize(m_description->width(), 40);
        }
    }

    if (nameEditor == NameEditor::Checkbox or nameEditor == NameEditor::NotExisting)
    {
        m_name->hide();
        m_description->hide();
    }

    if(isFinite)
    {
       m_duration = Utility::addFormToContainerWidget<WSpinBox>("Czas trwania zmiany statusu:", this);
       m_duration->setMinimum(1);
       m_duration->setMaximum(20);
       m_duration->setValue(1);
    }


    auto buttonContainer = addWidget(std::make_unique<WContainerWidget>());
    auto buttonLayout = buttonContainer->setLayout(std::make_unique<WHBoxLayout>());
    auto addButton = buttonLayout->addWidget(std::make_unique<WPushButton>("Dodaj parametr"));
    addButton->clicked().connect(this, &StatusChangeForm::addParam);
    auto rmButton = buttonLayout->addWidget(std::make_unique<WPushButton>("Usuń parametr"));
    rmButton->clicked().connect(this, &StatusChangeForm::rmParam);

    m_paramContainer = addWidget(std::make_unique<WContainerWidget>());
    addParam();
}

void StatusChangeForm::addParam()
{
    if (m_parameters.size() > 9)
    {
        return;
    }

    auto paramChange = m_paramContainer->addWidget(std::make_unique<ParameterChangeForm>());
    m_parameters.emplace_back(paramChange);
}

void StatusChangeForm::rmParam()
{
    if (m_parameters.empty())
    {
        return;
    }

    m_paramContainer->removeWidget(m_parameters.back().get());
    m_parameters.pop_back();
}

Dbo::ptr<StatusChange> StatusChangeForm::create(const WString& name) const
{
    auto& session = LancelotApp::session();

    using DurationOpt = boost::optional<int>;
    DurationOpt duration = m_duration ? m_duration->value():
                                        DurationOpt(boost::none);

    auto statName = m_name->isHidden() ? name.toUTF8() : m_name->valueText().toUTF8();
    auto description = m_description->isHidden() ? "-" : m_description->valueText().toUTF8();

    auto statChange = session.add(std::make_unique<StatusChange>(
            statName,
            description,
            duration));

    for(const auto& parameter : m_parameters)
    {
       auto dbParam = session.add(std::make_unique<ParameterModel>(parameter->getChosenValue()));
       statChange.modify()->params.insert(dbParam);
    }

    return statChange;
}

bool StatusChangeForm::validate() const
{
    auto allParametersValidated = m_parameters.empty() or
            std::all_of(std::begin(m_parameters), std::end(m_parameters),
                        [](auto parameter) { return parameter->validate(); });

    auto durationValid = m_duration ?
            m_duration->validate() == ValidationState::Valid : true;

    auto nameValid = m_name->isHidden() or
         (not m_name->text().empty() and m_name->validate() == ValidationState::Valid);

    return allParametersValidated and
           nameValid and
           durationValid;
}

void StatusChangeForm::showValidationError()
{
    WMessageBox::show("Błąd",
                      "Niepoprawna wartość w nazwie zmiany statusu, albo w którymś z parametrów.",
                      StandardButton::Ok);

}

}
