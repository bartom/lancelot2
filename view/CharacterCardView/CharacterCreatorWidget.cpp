#include "CharacterCreatorWidget.hpp"
#include <Wt/WVBoxLayout.h>
#include <Wt/WText.h>
#include <Wt/WPushButton.h>
#include "GeneralInfoWidget.hpp"
#include "ParametersWidget.hpp"
#include "LancelotApp.hpp"
#include "CommonModelsFillers.hpp"
#include "Character.hpp"
#include "Equipment.hpp"
#include "StatusChange.hpp"

namespace Lancelot
{
using namespace Wt;

CharacterCreatorWidget::CharacterCreatorWidget()
{
    setStyleClass("jumbotron");
    auto mainLayout = setLayout(std::make_unique<WVBoxLayout>());
    auto name = mainLayout->addWidget(std::make_unique<WText>());
    name->setText(WString("<h2 style=\"text-align:center\">Nowa postać</h2>"));

    m_generalInfo = mainLayout->addWidget(std::make_unique<GeneralInfoWidget>(nullptr));
    m_parameters = mainLayout->addWidget(std::make_unique<ParametersWidget>(std::nullopt));
    auto createButton = mainLayout->addWidget(std::make_unique<WPushButton>("Stwórz postać!"));
    createButton->clicked().connect(this, &CharacterCreatorWidget::createCharacter);
}

void CharacterCreatorWidget::update()
{
    m_generalInfo->update();
}

void CharacterCreatorWidget::createCharacter()
{
    auto name = m_generalInfo->getName().toUTF8();

    if (not validateChoices(name))
    {
        return;
    }

    auto& session = LancelotApp::session();
    Dbo::Transaction t(session);
    auto& filler = AgregateModelsFiller::instance();

    auto paramValues = m_parameters->getParamsFromWidgetValues();
    LifeParameters baseParams{paramValues.health(), paramValues.mana(), paramValues.stamina()};

    auto character = session.add(
            std::make_unique<Character>(name, m_generalInfo->getDeity(), baseParams));
    character.modify()->user = session.user();
    character.modify()->karma() = m_generalInfo->getKarma();
    character.modify()->level() = m_generalInfo->getLevel();
    character.modify()->exp() = m_generalInfo->getExp();

    auto race = m_generalInfo->getRace();
    character.modify()->race = filler.races[race];

    character.modify()->techClass = filler.techClasses[m_generalInfo->getTechClass()];

    character.modify()->mageClass = filler.mageClasses[m_generalInfo->getMageClass()];

    for(const auto& ptr : filler.races[race]->changes)
    {
        character.modify()->changes.insert(ptr);
    }
    log("notice") << "Created character";

    auto params = session.add(std::make_unique<ParameterPack>());
    params.modify()->setParams(paramValues);
    character.modify()->parameterPack = params;
    log("notice") << "Created param pack for character";

    auto eq = session.add(std::make_unique<Equipment>());
    character.modify()->equipment = eq;
    log("notice") << "Created equipment for character";

    auto pouch = session.add(std::make_unique<Pouch>(0, 0, 0, 0));
    character->equipment.modify()->pouch = pouch;
    log("notice") << "Created pouch";

    LancelotApp::instance()->setInternalPath("/card", true);
}

bool CharacterCreatorWidget::validateChoices(const std::string& name)
{
    if(name == "?" or name.empty())
    {
        WMessageBox::show("Błąd",
                          "Imię może zawierać tylko litery i spacje, "
                          "powinno zaczynać się z wielkiej litery.",
                          Wt::StandardButton::Ok);
        return false;
    }

    auto result = WMessageBox::show("Potwierdzenie",
                                    "Czy na pewno chcesz stworzyć postać o wybranych parametrach?",
                                    StandardButton::Yes | StandardButton::No);

    return result == StandardButton::Yes;

}


}
