#pragma once

#include <Wt/WDialog.h>
#include <Wt/WSpinBox.h>

namespace Lancelot
{
using namespace Wt::Core;

class ExpGainDialog: public Wt::WDialog
{
public:
    explicit ExpGainDialog();

    Wt::Signal<int> expAdded;

private:
    observing_ptr<Wt::WSpinBox> m_expBox;

};

}

