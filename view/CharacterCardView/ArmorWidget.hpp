#pragma once

#include "GenericItemWidget.hpp"
#include "Armor.hpp"

namespace Lancelot
{

class ArmorWidget: public GenericItemWidget<Armor>
{
public:
    explicit ArmorWidget(Wt::Dbo::ptr<Armor> armor);
    Wt::Signal<Wt::Dbo::ptr<Armor>>& equiped()
    { return m_equiped; }

private:
    void addDamageReductionAndArmorClassItems();
    void addArmorWearingItem();
    void showArmorDesctiption();
    Wt::WString createDescriptionContent();

    Wt::Signal<Wt::Dbo::ptr<Armor>> m_equiped;
};

}
