#include "StatusPanelWidget.hpp"
#include <Wt/WContainerWidget.h>
#include "LancelotApp.hpp"
#include "Parameter.hpp"
#include "ParameterModel.hpp"
#include "Character.hpp"
#include "Equipment.hpp"
#include "Armor.hpp"
#include "Jewellery.hpp"


namespace Lancelot
{
using namespace Wt;

StatusPanelWidget::StatusPanelWidget()
{
    setTitle("Zmiany statusu");
    setCollapsible(true);
    m_centralWidget = setCentralWidget(std::make_unique<WContainerWidget>());
}

void StatusPanelWidget::update(const Wt::Dbo::ptr<Character>& character)
{
    m_centralWidget->clear();
    Dbo::Transaction t(LancelotApp::session());

    const auto& changes = character.modify()->changes;
    for (const auto& change : changes)
    {
        auto charChange = m_centralWidget->addWidget(std::make_unique<StatusChangeWidget>(change));
        charChange->removed.connect([this](){ changed.emit(); });
    }

    const auto& armor = character->equipment.modify()->armor;
    for (const auto& armorPiece : armor)
    {
        if (armorPiece->isEquipped())
        {
            if (auto statChange = armorPiece->statusChange;
                not (statChange->description() == "-" and statChange->params.empty()))
            {
                m_centralWidget->addWidget(std::make_unique<StatusChangeWidget>(statChange));
            }
        }
    }

    const auto& jewellery = character->equipment.modify()->jewellery;
    for (const auto& jewelleryPiece: jewellery)
    {
        if (jewelleryPiece->isEquipped())
        {
            if (auto statChange = jewelleryPiece->statusChange;
                    not (statChange->description() == "-" and statChange->params.empty()))
            {
                m_centralWidget->addWidget(std::make_unique<StatusChangeWidget>(statChange));
            }
        }
    }
}

}
