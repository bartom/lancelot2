#pragma once

#include <Wt/WDialog.h>
#include <Wt/WSignal.h>
#include "Character.hpp"

namespace Lancelot
{
class StatusChangeForm;

class NewStatusChangeDialog : public Wt::WDialog
{
public:
    explicit NewStatusChangeDialog(Wt::Dbo::ptr<Character> character);

    Wt::Signal<> statusChangeCreated;

private:
    void createFromView();

    Wt::Dbo::ptr<Character> m_character;
    StatusChangeForm* m_statusChangeForm;
};

}
