#include <utility>

#include "JewelleryWidget.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"

namespace Lancelot
{
using namespace Wt;

JewelleryWidget::JewelleryWidget(Wt::Dbo::ptr<Jewellery> jewellery):
    GenericItemWidget(std::move(jewellery))
{
    m_menu->insertItem(0, "Opis")->clicked().connect(
            this, &JewelleryWidget::showJewelleryDesctiption);
    addJewelleryWearingItem();
}

void JewelleryWidget::showJewelleryDesctiption()
{
    WMessageBox::show(m_item->name(),
        createDescriptionContent(),
        StandardButton::Ok);
}

Wt::WString JewelleryWidget::createDescriptionContent()
{
    auto content = WString("<p>{1}</p>").arg(m_item->description());

    Dbo::Transaction t(LancelotApp::session());
    if(auto change = m_item->statusChange; change)
    {
        content += WString("<br/>");
        content += WString("<p>Efekt: {1}</p>").arg(change->name());
        if(not change->params.empty())
        {
            content += WString("<p>Zmiany w parametrach:</p>");
            for (auto it = change->params.begin(); it != change->params.end(); ++it)
            {
                const auto[param, value] = (*it)->typeToValue;
                content += WString("<p>{1}: {2}</p>").arg(toString(param)).arg(value);
            }
        }
    }

    return content;
}

void JewelleryWidget::addJewelleryWearingItem()
{
    auto label = m_item->isEquipped() ? "Zdejmij" : "Załóż";

    m_menu->insertItem(1, label)->clicked().connect([&](){
        m_equiped.emit(m_item);
    });
}

}
