#pragma once

#include "GenericItemWidget.hpp"
#include "Potion.hpp"

namespace Lancelot
{

class PotionWidget : public GenericItemWidget<Potion>
{
public:
    explicit PotionWidget(Wt::Dbo::ptr<Potion> potion);
    Wt::Signal<Wt::Dbo::ptr<StatusChange>> used;

private:
    void showPotionDesctiption();
    void use();
    Wt::WString createDescriptionContent() const;

};

}
