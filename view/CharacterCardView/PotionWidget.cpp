#include <utility>

#include "PotionWidget.hpp"
#include <Wt/WString.h>
#include <Character.hpp>
#include "StatusChange.hpp"
#include "LancelotApp.hpp"
#include "ParameterModel.hpp"

namespace Lancelot
{
using namespace Wt;

PotionWidget::PotionWidget(Wt::Dbo::ptr<Potion> potion):
    GenericItemWidget(std::move(potion))
{
    m_menu->insertItem(0, "Opis")->clicked().connect(
            this, &PotionWidget::showPotionDesctiption);

    m_menu->insertItem(1, "Użyj")->clicked().connect(
            this, &PotionWidget::use);
}

void PotionWidget::showPotionDesctiption()
{
    const static auto descContent = createDescriptionContent();
    Wt::WMessageBox::show(m_item->name(),
            descContent,
            Wt::StandardButton::Ok);
}

void PotionWidget::use()
{
    Dbo::Transaction t(LancelotApp::session());
    auto statChange = m_item->statusChange;

    if (not Character::potionUsedThisTurn)
    {
        if (m_item->amount() == 1)
        {
            m_item.modify()->statusChange = nullptr;
            removeItem();
        }
        else
        {
            m_item.modify()->setAmount(m_item->amount()-1);
        }
    }

    used.emit(statChange);
}


Wt::WString PotionWidget::createDescriptionContent() const
{
    auto content = WString("<p>{1}</p>").arg(m_item->description());

    Dbo::Transaction t(LancelotApp::session());
    if(auto change = m_item->statusChange; change)
    {
        content += WString("<br/>");
        if(auto duration = change->duration(); duration.is_initialized())
        {
            content += WString("<p>Czas działania: {1} tur</p>").arg(duration.get());
        }

        if(not change->params.empty())
        {
            content += WString("<p>Efekty:</p>");
            for (auto it = change->params.begin(); it != change->params.end(); ++it)
            {
                const auto[param, value] = (*it)->typeToValue;
                content += WString("<p>{1}: {2}</p>").arg(toString(param)).arg(value);
            }
        }
    }

    return content;
}

}
