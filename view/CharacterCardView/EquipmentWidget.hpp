#pragma once

#include <Wt/WPanel.h>
#include <Wt/WContainerWidget.h>
#include <Wt/Dbo/collection.h>
#include "Equipment.hpp"
#include "StatusChange.hpp"

namespace Lancelot
{
using namespace Wt::Core;

struct EquippedWeapons
{
    Wt::Dbo::ptr<Weapon> mainHand;
    Wt::Dbo::ptr<Weapon> offHand;
    Wt::Dbo::ptr<Weapon> inQuiver;

    void findEquippedWeapons(const Wt::Dbo::collection<Wt::Dbo::ptr<Weapon>>& weapons);
    void weaponChange(const Wt::Dbo::ptr<Weapon>& weapon);
    static bool isMainHand(WeaponType type);
    static bool isOffHand(WeaponType type);
    static bool isInQuiver(WeaponType type);

private:
    void weaponChangeImpl(Wt::Dbo::ptr<Weapon>& equippedWeapon,
                          const Wt::Dbo::ptr<Weapon>& weaponToEquip);
};

struct EquippedArmor
{
    std::map<ArmorType, Wt::Dbo::ptr<Armor>> onCharacter;

    void findEquippedArmor(const Wt::Dbo::collection<Wt::Dbo::ptr<Armor>>& armor);
    void armorChange(const Wt::Dbo::ptr<Armor>& armorPiece);
};

struct EquippedJewellery
{
    Wt::Dbo::ptr<Jewellery> amulet;
    Wt::Dbo::ptr<Jewellery> leftRing;
    Wt::Dbo::ptr<Jewellery> rightRing;

    void findEquippedJewellery(const Wt::Dbo::collection<Wt::Dbo::ptr<Jewellery>>& jewellery);
    void jewelleryChange(const Wt::Dbo::ptr<Jewellery>& jewelleryPiece);

private:
    void hadleUnequip(const Wt::Dbo::ptr<Jewellery>& jewelleryPiece);
    void hadleEquip(const Wt::Dbo::ptr<Jewellery>& jewelleryPiece);
};

class EquipmentWidget: public Wt::WContainerWidget
{
public:
    explicit EquipmentWidget(Wt::Dbo::ptr<Equipment> eq);
    void update();
    Wt::Signal<> equipmentChanged;
    Wt::Signal<Wt::Dbo::ptr<StatusChange>> potionUsed;

private:
    observing_ptr<Wt::WContainerWidget> createPanelWithContainer(const std::string& title);
    void updateWeapons();
    void updateWearable();
    void updatePotions();
    void updateItems();

    void handleWeaponChange(Wt::Dbo::ptr<Weapon> weapon);
    void handleArmorChange(Wt::Dbo::ptr<Armor> armor);
    void handleJewelleryChange(Wt::Dbo::ptr<Jewellery> jewellery);

    Wt::Dbo::ptr<Equipment> m_eq;
    observing_ptr<Wt::WVBoxLayout> m_layout;

    observing_ptr<Wt::WContainerWidget> m_weaponContainer;
    observing_ptr<Wt::WContainerWidget> m_wearableContainer;
    observing_ptr<Wt::WContainerWidget> m_potionContainer;
    observing_ptr<Wt::WContainerWidget> m_itemContainer;

    EquippedWeapons m_equippedWeapons;
    EquippedJewellery m_equippedJewellery;
    EquippedArmor m_equippedArmor;
};

}
