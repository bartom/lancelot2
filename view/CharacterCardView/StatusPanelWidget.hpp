#pragma once

#include <Wt/WPanel.h>
#include "StatusChange.hpp"
#include "StatusChangeWidget.hpp"

namespace Lancelot
{
class Parameters;

class StatusPanelWidget: public Wt::WPanel
{
public:
    explicit StatusPanelWidget();
    void update(const Wt::Dbo::ptr<Character>& character);
    Wt::Signal<> changed;

private:
    Wt::WContainerWidget* m_centralWidget;

};

}
