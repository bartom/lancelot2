#pragma once

#include <Wt/WDialog.h>
#include <Wt/WSpinBox.h>
#include "Character.hpp"
#include "Parameter.hpp"

namespace Lancelot
{
using namespace Wt::Core;

class TurnEndDialog : public Wt::WDialog
{
public:
    explicit TurnEndDialog();

    Wt::Signal<LifeParameters> turnEnded;

private:
    observing_ptr<Wt::WSpinBox> m_healthBox;
    observing_ptr<Wt::WSpinBox> m_manaBox;
    observing_ptr<Wt::WSpinBox> m_staminaBox;

    void endTurn();
};


}


