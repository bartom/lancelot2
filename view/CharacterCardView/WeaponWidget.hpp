#pragma once

#include "GenericItemWidget.hpp"
#include "Weapon.hpp"

namespace Lancelot
{

class WeaponWidget : public GenericItemWidget<Weapon>
{
public:
    explicit WeaponWidget(Wt::Dbo::ptr<Weapon> weapon);
    Wt::Signal<Wt::Dbo::ptr<Weapon>>& equiped()
    { return m_equiped; }

private:
    Wt::Signal<Wt::Dbo::ptr<Weapon>> m_equiped;
    void addWeaponDamageItem();
    void addDurabilityItem();
    void addWeaponDescription();
    void addWeaponWearingItem();

};

}
