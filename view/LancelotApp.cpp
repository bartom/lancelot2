#include "LancelotApp.hpp"
#include <Wt/WText.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WAnchor.h>
#include <Wt/WLink.h>
#include <Wt/Dbo/ptr.h>
#include <Wt/WNavigationBar.h>
#include <Wt/WStackedWidget.h>
#include "Character.hpp"
#include "LancelotWidget.hpp"

namespace Lancelot
{
using namespace Wt;
using namespace Wt::Auth;


LancelotApp::LancelotApp(const WEnvironment& env):
    WApplication(env),
    m_session(appRoot() + "lancelot.db")
{
    // IT'S PERFECTLY SAFE, OK?!
    g_session = &m_session;

    changeTheme();
    setTitle("LANCELOT2");

    m_mainStack = root()->addWidget(std::make_unique<WStackedWidget>());

    addAuthWidget();
    addLancelotWidget();

    m_mainStack->setCurrentWidget(m_externalAuthContainer.get());

    m_session.login().changed().connect(this, &LancelotApp::handleAuthEvent);
    internalPathChanged().connect(this, &LancelotApp::handleInternalPathChangedEvent);

    handleAuthEvent();
}

void LancelotApp::handleAuthEvent()
{
    if(m_session.login().loggedIn())
    {
        const Auth::User& user = m_session.login().user();
        log("notice") << "User " << user.id()
        << " (" << user.identity(Auth::Identity::LoginName) << ")"
        << " logged in.";

        m_mainStack->setCurrentWidget(m_appWidget.get());
    }
    else
    {
        m_mainStack->setCurrentWidget(m_externalAuthContainer.get());
        log("notice") << "User not logged in.";
    }
    handleInternalPathChangedEvent(internalPath());
}

void LancelotApp::handleInternalPathChangedEvent(const std::string &internalPath)
{
    if(m_session.login().loggedIn())
    {
        // passing path to internal widget handler
        m_appWidget->handleInternalPathChange(internalPath);
    }
    else if (internalPath != "/")
    {
        setInternalPath("/");
    }
}

std::unique_ptr<WApplication> LancelotApp::createApp(const WEnvironment& env)
{
    return std::make_unique<LancelotApp>(env);
}

void LancelotApp::changeTheme()
{
    //useStyleSheet("resources/themes/bootstrap/3/bootstrap-theme.min.css");
    useStyleSheet("css/style.css");
    useStyleSheet("css/theme2.css");
    root()->addStyleClass("container");
    auto theme = std::make_shared<WBootstrapTheme>();
    theme->setVersion(BootstrapVersion::v3);
    theme->setResponsive(true);
    setTheme(theme);

    messageResourceBundle().use(appRoot() + "text");
}

void LancelotApp::addAuthWidget()
{
    m_externalAuthContainer =
            m_mainStack->addWidget(std::make_unique<WContainerWidget>());
    m_externalAuthContainer->addWidget(std::make_unique<WBreak>());
    auto authWidget = std::make_unique<AuthWidget>(Session::authService(),
                                                   m_session.users(),
                                                   m_session.login());
    authWidget->model()->addPasswordAuth(&Session::passwordService());
    authWidget->setRegistrationEnabled(true);
    authWidget->processEnvironment();
    m_externalAuthContainer->addWidget(std::move(authWidget));

}

void LancelotApp::addLancelotWidget()
{
    m_appWidget = m_mainStack->addWidget(
                std::make_unique<LancelotWidget>());
}

Session& LancelotApp::session()
{
    // as I said, it's save...
    if(not g_session)
    {
        throw std::runtime_error("Empty session in LancelotApp!");
    }
    // ...probably
    return *g_session;
}


}
