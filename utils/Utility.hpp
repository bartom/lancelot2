#pragma once

#include <Wt/WGridLayout.h>
#include <Wt/WTemplate.h>
#include <Wt/WString.h>

namespace Lancelot::Utility
{

constexpr char POLISH_UPPER[] = "ĄĆĘŁŃÓŚŻŹ";
constexpr char POLISH_LOWER[] = "ąćęłńóśżź";
const Wt::WString NAME_REGEX_NONUMBER =
        Wt::WString("[A-Z{1}][a-zA-Z{2}{3}\\s]*").arg(POLISH_UPPER).arg(POLISH_UPPER).arg(POLISH_LOWER);
const Wt::WString NAME_REGEX =
        Wt::WString("[A-Z{1}0-9][a-zA-Z0-9{2}{3}\\s]*").arg(POLISH_UPPER).arg(POLISH_UPPER).arg(POLISH_LOWER);

template <typename MapType, typename ...Ts, typename ValueType = int>
void valueEmplacer(MapType& map, ValueType val, Ts&& ... keys)
{
    (map.emplace(keys, val), ...);
}

template<typename Form>
Form* addFormToGridLayout(Wt::WGridLayout* layout, const std::string &label,
                          size_t row, size_t col, size_t length=0, bool setReadOnly = true)
{
    auto form = layout->addWidget(
            std::make_unique<Wt::WTemplate>(Wt::WString::tr("lineEdit-template")),
            row, col, 0, length, Wt::AlignmentFlag::Left);
    form->addFunction("id", &Wt::WTemplate::Functions::id);

    form->bindString("label", label);
    auto formWidget = form->bindWidget("edit", std::make_unique<Form>());

    formWidget->setReadOnly(setReadOnly);

    return formWidget;
}

template <typename Form, typename Container>
Form* addFormToContainerWidget(const char* labelContent, Container* container)
{
    auto label = container->addWidget(std::make_unique<Wt::WLabel>(labelContent));
    auto form = container->addWidget(std::make_unique<Form>());
    label->setBuddy(form);
    form->setMargin(7, Wt::Side::Bottom);
    return form;
}


}
