#include "CommonModelsFillers.hpp"
#include "StatusChange.hpp"
#include "ParameterModel.hpp"


namespace Lancelot
{
using namespace Wt;
using namespace std::string_literals;

using Races = Dbo::collection<Dbo::ptr<Race>>;
using TechClasses = Dbo::collection<Dbo::ptr<TechClass>>;
using MageClasses = Dbo::collection<Dbo::ptr<MageClass>>;

TechClassFillers::TechClassFillers():
    m_session(LancelotApp::session())
{
    Dbo::Transaction t(m_session);

    TechClasses techClasses = m_session.find<TechClass>();

    if (not techClasses.empty())
    {
        getExistingTechClassesFromDB();
    }
    else
    {
        addBerserker();
        addHeavyArmed();
        addBladeDancer();
        addAgent();
        addLancer();
        addScout();
    }
}

void TechClassFillers::addBerserker()
{
    auto berserker = m_session.add(std::make_unique<TechClass>(TechClassEnum::Berserker,
                                                               ArmorClass::Light, Parameter::Strength));
    berserker.modify()->addWeaponType(WeaponType::TwoHAxe);
    berserker.modify()->addWeaponType(WeaponType::TwoHSword);
    berserker.modify()->addWeaponType(WeaponType::TwoHHammer);
    techClasses[TechClassEnum::Berserker] = berserker;
}

void TechClassFillers::addHeavyArmed()
{
    auto heavy = m_session.add(std::make_unique<TechClass>(TechClassEnum::HeavyArmed,
                                                           ArmorClass::Heavy, Parameter::Strength));
    heavy.modify()->addWeaponType(WeaponType::Axe);
    heavy.modify()->addWeaponType(WeaponType::Sword);
    heavy.modify()->addWeaponType(WeaponType::Hammer);
    heavy.modify()->addWeaponType(WeaponType::TwoHAxe);
    heavy.modify()->addWeaponType(WeaponType::TwoHSword);
    heavy.modify()->addWeaponType(WeaponType::TwoHHammer);
    techClasses[TechClassEnum::HeavyArmed] = heavy;
}

void TechClassFillers::addBladeDancer()
{
    auto dancer = m_session.add(std::make_unique<TechClass>(TechClassEnum::BladeDancer,
                                                            ArmorClass::Medium, Parameter::Strength));
    dancer.modify()->addWeaponType(WeaponType::Sword);
    techClasses[TechClassEnum::BladeDancer] = dancer;
}

void TechClassFillers::addAgent()
{
    auto agent = m_session.add(std::make_unique<TechClass>(TechClassEnum::Agent,
                                                           ArmorClass::Light, Parameter::Agility));
    agent.modify()->addWeaponType(WeaponType::Dagger);
    agent.modify()->addWeaponType(WeaponType::Thrown);
    techClasses[TechClassEnum::Agent] = agent;
}

void TechClassFillers::addLancer()
{

    auto lancer = m_session.add(std::make_unique<TechClass>(TechClassEnum::Lancer,
                                                            ArmorClass::Medium, Parameter::Agility));
    lancer.modify()->addWeaponType(WeaponType::Lance);
    techClasses[TechClassEnum::Lancer] = lancer;
}

void TechClassFillers::addScout()
{
    auto scout = m_session.add(std::make_unique<TechClass>(TechClassEnum::Scout,
                                                           ArmorClass::Light, Parameter::Agility));
    scout.modify()->addWeaponType(WeaponType::Bow);
    scout.modify()->addWeaponType(WeaponType::Crossbow);
    techClasses[TechClassEnum::Scout] = scout;
}


void TechClassFillers::getExistingTechClassesFromDB()
{
    for(auto techClass: TECH_CLASSES)
    {
        techClasses[techClass] = m_session.find<TechClass>().where("tech_class_enum = ?"s).bind(techClass);
    }

}

MageClassFillers::MageClassFillers():
    m_session(LancelotApp::session())
{
    Dbo::Transaction t(m_session);

    MageClasses mageClasses = m_session.find<MageClass>();

    if (not mageClasses.empty())
    {
        getExistingMageClassesFromDB();
    }
    else
    {
        addAlchemist();
        addEngineer();
        addEnhancer();
        addMistique();
        addSummoner();
        addShaman();
    }
}

void MageClassFillers::addAlchemist()
{
    auto alchemist = m_session.add(std::make_unique<MageClass>(
            MageClassEnum::Alchemist, Parameter::Intelligence));
    fillEveryDeity(alchemist);
    mageClasses[MageClassEnum::Alchemist] = alchemist;
}

void MageClassFillers::addEngineer()
{
    auto engineer = m_session.add(std::make_unique<MageClass>(
            MageClassEnum::Engineer, Parameter::Intelligence));
    fillEveryDeity(engineer);
    mageClasses[MageClassEnum::Engineer] = engineer;
}

void MageClassFillers::addEnhancer()
{
    auto enhancer = m_session.add(std::make_unique<MageClass>(
            MageClassEnum::Enhancer, Parameter::Intelligence));
    fillEveryDeity(enhancer);
    mageClasses[MageClassEnum::Enhancer] = enhancer;
}

void MageClassFillers::addMistique()
{
    auto mistique = m_session.add(std::make_unique<MageClass>(
            MageClassEnum::Mistique, Parameter::Charisma));
    fillEveryDeity(mistique);
    mageClasses[MageClassEnum::Mistique] = mistique;
}

void MageClassFillers::addSummoner()
{
    auto summoner = m_session.add(std::make_unique<MageClass>(
            MageClassEnum::Summoner, Parameter::Charisma));
    fillEveryDeity(summoner);
    mageClasses[MageClassEnum::Summoner] = summoner;
}

void MageClassFillers::addShaman()
{
    auto shaman = m_session.add(std::make_unique<MageClass>(
            MageClassEnum::Shaman, Parameter::Charisma));
    shaman.modify()->addDeity(Deity::Laselis);
    shaman.modify()->addDeity(Deity::Nateis);
    shaman.modify()->addDeity(Deity::Ferlar);
    shaman.modify()->addDeity(Deity::Dethelion);
    mageClasses[MageClassEnum::Shaman] = shaman;
}

void MageClassFillers::fillEveryDeity(Wt::Dbo::ptr<MageClass>& mageClass)
{
    mageClass.modify()->addDeity(Deity::Aquest);
    mageClass.modify()->addDeity(Deity::Arles);
    mageClass.modify()->addDeity(Deity::Dethelion);
    mageClass.modify()->addDeity(Deity::Ferlar);
    mageClass.modify()->addDeity(Deity::Laselis);
    mageClass.modify()->addDeity(Deity::Nateis);
}

void MageClassFillers::getExistingMageClassesFromDB()
{
    for(auto mageClass: MAGE_CLASSES)
    {
        mageClasses[mageClass] = m_session.find<MageClass>().where("mage_class_enum = ?"s).bind(mageClass);
    }
}

RaceFillers::RaceFillers(const TechClassArray& techClasses, const MageClassArray& mageClasses):
    m_session(LancelotApp::session()),
    m_techClasses(techClasses),
    m_mageClasses(mageClasses)
{
    Dbo::Transaction t(m_session);

    Races races = m_session.find<Race>();

    if (not races.empty())
    {
        getExistingRacesFromDB();
    }
    else
    {
        addHuman();
        addRhoden();
        addItichan();
        addElf();
        addDwarf();
        addDrow();
    }
}

void RaceFillers::addHuman()
{
    auto human = m_session.add(std::make_unique<Race>(RaceEnum::Human));
    for(const auto& techClass: m_techClasses)
    {
        human.modify()->techClasses.insert(techClass);
    }
    for(const auto& mageClass: m_mageClasses)
    {
        human.modify()->mageClasses.insert(mageClass);
    }
    auto humanStatus = m_session.add(std::make_unique<StatusChange>(toString(RaceEnum::Human),
            "Dodatkowy punkt umiejętności co 3 poziomy"s));
    human.modify()->changes.insert(humanStatus);
    races[RaceEnum::Human] = human;
}

void RaceFillers::addRhoden()
{
    auto rhoden = m_session.add(std::make_unique<Race>(RaceEnum::Rhoden));
    for(auto techClass: {TechClassEnum::Berserker, TechClassEnum::HeavyArmed,
                         TechClassEnum::BladeDancer, TechClassEnum::Lancer})
    {
        rhoden.modify()->techClasses.insert(m_techClasses[techClass]);
    }
    for(auto mageClass: {MageClassEnum::Mistique, MageClassEnum::Summoner, MageClassEnum::Shaman})
    {
        rhoden.modify()->mageClasses.insert(m_mageClasses[mageClass]);
    }

    auto rhodenStatus = m_session.add(std::make_unique<StatusChange>(toString(RaceEnum::Rhoden),
            "-"s));
    auto strengthBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Strength, 2));
    auto agilityBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Agility, 2));
    auto intelligencePenalty = m_session.add(std::make_unique<ParameterModel>(Parameter::Intelligence, -1));
    rhodenStatus.modify()->params.insert(strengthBoost);
    rhodenStatus.modify()->params.insert(agilityBoost);
    rhodenStatus.modify()->params.insert(intelligencePenalty);

    auto clawsStatus = m_session.add(std::make_unique<StatusChange>("Szpony"s,
            "Potężne szpony zapewniają 1k4 obrażeń w przypadku walki wręcz."s));
    auto tailStatus = m_session.add(std::make_unique<StatusChange>("Ogon"s,
            "Długi na około metr, zakończony grotem ogon."s));
    auto hornStatus = m_session.add(std::make_unique<StatusChange>("Rogi"s,
            "Krótkie, wyłaniające się spomiędzy włosów rogi."s));

    rhoden.modify()->changes.insert(rhodenStatus);
    rhoden.modify()->changes.insert(clawsStatus);
    rhoden.modify()->changes.insert(tailStatus);
    rhoden.modify()->changes.insert(hornStatus);

    races[RaceEnum::Rhoden] = rhoden;
}

void RaceFillers::addItichan()
{
    auto itichan = m_session.add(std::make_unique<Race>(RaceEnum::Itichan));

    itichan.modify()->techClasses.insert(m_techClasses[TechClassEnum::Agent]);
    itichan.modify()->techClasses.insert(m_techClasses[TechClassEnum::Scout]);
    for(auto mageClass: {MageClassEnum::Alchemist, MageClassEnum::Engineer,
                         MageClassEnum::Enhancer, MageClassEnum::Mistique})
    {
        itichan.modify()->mageClasses.insert(m_mageClasses[mageClass]);
    }

    auto itichanStatus = m_session.add(std::make_unique<StatusChange>(toString(RaceEnum::Itichan),
                                                                     "-"s));
    auto agilityBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Agility, 1));
    auto intelligenceBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Intelligence, 2));
    itichanStatus.modify()->params.insert(agilityBoost);
    itichanStatus.modify()->params.insert(intelligenceBoost);

    auto lungGillsStatus = m_session.add(std::make_unique<StatusChange>("Płuco-skrzela"s,
            "Dzięki narządowi łączącemu funkcje płuc oraz skrzeli, itichanie są w stanie"
            " oddychać pod wodą nawet kilka dni."));
    auto interdigitalWebbingStatus = m_session.add(std::make_unique<StatusChange>("Błona pławna"s,
            "Skórzasta struktura rozpięta między palcami, pozwalająca na łatwiejsze poruszanie"
            " się w wodzie."));
    itichan.modify()->changes.insert(itichanStatus);
    itichan.modify()->changes.insert(lungGillsStatus);
    itichan.modify()->changes.insert(interdigitalWebbingStatus);

    races[RaceEnum::Itichan] = itichan;
}

void RaceFillers::addElf()
{
    auto elf = m_session.add(std::make_unique<Race>(RaceEnum::Elf));

    for(auto techClass: {TechClassEnum::BladeDancer, TechClassEnum::Agent, TechClassEnum::Scout})
    {
        elf.modify()->techClasses.insert(m_techClasses[techClass]);
    }
    for(auto mageClass: {MageClassEnum::Alchemist, MageClassEnum::Enhancer,
                         MageClassEnum::Mistique, MageClassEnum::Summoner})
    {
        elf.modify()->mageClasses.insert(m_mageClasses[mageClass]);
    }

    auto elfStatus = m_session.add(std::make_unique<StatusChange>(toString(RaceEnum::Elf),
                                                                  "-"s));
    auto agilityBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Agility, 1));
    auto intelligenceBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Intelligence, 1));
    auto charismaBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Charisma, 1));
    elfStatus.modify()->params.insert(agilityBoost);
    elfStatus.modify()->params.insert(intelligenceBoost);
    elfStatus.modify()->params.insert(charismaBoost);

    auto infraVisonStatus = m_session.add(std::make_unique<StatusChange>("Infrawizja"s,
            "Zdolność do widzenia w ciemności w zakresie podczerwieni. Nie pozwala widzieć wszystkiego,"
            " niemniej jednak ułatwia dostrzeżenie potencjalnego zagrożenia."));
    elf.modify()->changes.insert(elfStatus);
    elf.modify()->changes.insert(infraVisonStatus);

    races[RaceEnum::Elf] = elf;
}

void RaceFillers::addDwarf()
{
    auto dwarf = m_session.add(std::make_unique<Race>(RaceEnum::Dwarf));

    for(auto techClass: {TechClassEnum::BladeDancer, TechClassEnum::Berserker, TechClassEnum::HeavyArmed})
    {
        dwarf.modify()->techClasses.insert(m_techClasses[techClass]);
    }
    for(auto mageClass: {MageClassEnum::Alchemist, MageClassEnum::Engineer, MageClassEnum::Enhancer})
    {
        dwarf.modify()->mageClasses.insert(m_mageClasses[mageClass]);
    }

    auto dwarfStatus = m_session.add(std::make_unique<StatusChange>(toString(RaceEnum::Dwarf),
                                                                    "-"s));
    auto strengthBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Strength, 1));
    auto agilityBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Agility, 1));
    auto healthBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Health, 2));
    dwarfStatus.modify()->params.insert(strengthBoost);
    dwarfStatus.modify()->params.insert(agilityBoost);
    dwarfStatus.modify()->params.insert(healthBoost);

    auto infraVisonStatus = m_session.add(std::make_unique<StatusChange>("Infrawizja"s,
            "Zdolność do widzenia w ciemności w zakresie podczerwieni. Nie pozwala widzieć wszystkiego,"
            " niemniej jednak ułatwia dostrzeżenie potencjalnego zagrożenia."));
    dwarf.modify()->changes.insert(dwarfStatus);
    dwarf.modify()->changes.insert(infraVisonStatus);

    races[RaceEnum::Dwarf] = dwarf;
}

void RaceFillers::addDrow()
{
    auto drow = m_session.add(std::make_unique<Race>(RaceEnum::Drow));

    drow.modify()->techClasses.insert(m_techClasses[TechClassEnum::Lancer]);
    drow.modify()->techClasses.insert(m_techClasses[TechClassEnum::Agent]);
    for(auto mageClass: {MageClassEnum::Alchemist, MageClassEnum::Mistique, MageClassEnum::Enhancer,
                         MageClassEnum::Summoner, MageClassEnum::Shaman})
    {
        drow.modify()->mageClasses.insert(m_mageClasses[mageClass]);
    }

    auto drowStatus = m_session.add(std::make_unique<StatusChange>(toString(RaceEnum::Drow),
                                                                    "-"s));
    auto strengthPenalty = m_session.add(std::make_unique<ParameterModel>(Parameter::Strength, -1));
    auto agilityPenalty = m_session.add(std::make_unique<ParameterModel>(Parameter::Agility, -1));
    auto manaBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Mana, 4));
    auto intelligenceBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::Intelligence, 2));
    auto magicAbilityBoost = m_session.add(std::make_unique<ParameterModel>(Parameter::MagicAbility, 1));
    drowStatus.modify()->params.insert(manaBoost);
    drowStatus.modify()->params.insert(intelligenceBoost);
    drowStatus.modify()->params.insert(strengthPenalty);
    drowStatus.modify()->params.insert(agilityPenalty);
    drowStatus.modify()->params.insert(magicAbilityBoost);

    auto infraVisonStatus = m_session.add(std::make_unique<StatusChange>("Infrawizja"s,
            "Zdolność do widzenia w ciemności w zakresie podczerwieni. Nie pozwala widzieć wszystkiego,"
            " niemniej jednak ułatwia dostrzeżenie potencjalnego zagrożenia."));
    auto hornStatus = m_session.add(std::make_unique<StatusChange>("Rogi"s,
            "Krótkie, wyłaniające się spomiędzy włosów rogi."s));
    drow.modify()->changes.insert(drowStatus);
    drow.modify()->changes.insert(infraVisonStatus);
    drow.modify()->changes.insert(hornStatus);

    races[RaceEnum::Drow] = drow;
}


void RaceFillers::getExistingRacesFromDB()
{
    for(auto race: RACES)
    {
        races[race] = m_session.find<Race>().where("race_enum = ?"s).bind(race);
    }
}

AgregateModelsFiller::AgregateModelsFiller():
    m_techClassFillers(),
    m_mageClassFillers(),
    m_raceFillers(m_techClassFillers.techClasses, m_mageClassFillers.mageClasses),
    techClasses(m_techClassFillers.techClasses),
    mageClasses(m_mageClassFillers.mageClasses),
    races(m_raceFillers.races)
{}

AgregateModelsFiller& AgregateModelsFiller::instance()
{
    static AgregateModelsFiller instance;
    return instance;
}

}
