
#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include "ItemBase.hpp"
#include "CommonEnums.hpp"

namespace Lancelot
{
class Equipment;
class StatusChange;

class Jewellery : public ItemBase
{
public:
    Jewellery();

    Jewellery(std::string name, std::string description, int amount,
              double singularWeight, JewelleryType type);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_name, "name"s);
        Wt::Dbo::field(a, m_description, "description"s);
        Wt::Dbo::field(a, m_amount, "amount"s);
        Wt::Dbo::field(a, m_singularWeight, "weight"s);

        Wt::Dbo::field(a, m_type, "type"s);
        Wt::Dbo::field(a, m_isEquipped, "equipped"s);

        Wt::Dbo::hasOne(a, statusChange);
        Wt::Dbo::belongsTo(a, m_eq, "equipment"s);
    }

    Wt::Dbo::weak_ptr<StatusChange> statusChange;

    JewelleryType type() const
    { return m_type; }

    bool isEquipped() const
    { return m_isEquipped; }

    void flipEquippedState();

private:
    JewelleryType m_type;
    bool m_isEquipped;

    Wt::Dbo::ptr<Equipment> m_eq;
};

}

DBO_EXTERN_TEMPLATES(Lancelot::Jewellery)