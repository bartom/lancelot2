#include "Pouch.hpp"
#include <Wt/Dbo/Impl.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "Equipment.hpp"

namespace Lancelot
{

Pouch::Pouch(int platinum, int gold, int silver, int copper):
    m_coins{{CoinType::Platinum, platinum},
            {CoinType::Gold, gold},
            {CoinType::Silver, silver},
            {CoinType::Copper, copper}}
{
}

Pouch::Pouch():
   Pouch(0, 0, 0, 0)
{}

void Pouch::applyDelta(PouchDelta delta)
{
    applyCoinChange(delta.copper, CoinType::Copper, CoinType::Silver);
    applyCoinChange(delta.silver, CoinType::Silver, CoinType::Gold);
    applyCoinChange(delta.gold, CoinType::Gold, CoinType::Platinum);
    auto& plat = m_coins.at(CoinType::Platinum);
    plat = std::max(0, plat+delta.plat);
}

void Pouch::applyCoinChange(int amount, CoinType lesserNominal, CoinType higherNominal)
{
    auto& lesser = m_coins.at(lesserNominal);
    lesser = std::max(0, lesser+amount);
    if (lesser >= 10)
    {
        lesser -= 10;
        m_coins.at(higherNominal) += 1;
    }
}


}

DBO_INSTANTIATE_TEMPLATES(Lancelot::Pouch)

