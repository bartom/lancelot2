#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <map>
#include "CommonEnums.hpp"
#include "PouchDelta.hpp"

namespace Lancelot
{
class Equipment;

class Pouch
{
public:
    Pouch();

    Pouch(int platinum, int gold, int silver, int copper);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;
        Wt::Dbo::field(a, m_coins.at(CoinType::Platinum), "platinum"s);
        Wt::Dbo::field(a, m_coins.at(CoinType::Gold), "gold"s);
        Wt::Dbo::field(a, m_coins.at(CoinType::Silver), "silver"s);
        Wt::Dbo::field(a, m_coins.at(CoinType::Copper), "copper"s);
        Wt::Dbo::belongsTo(a, m_eq, "equipment"s);
    }

    int coins(CoinType type) const
    { return m_coins.at(type); }

    void applyDelta(PouchDelta delta);

private:
    std::map<CoinType, int> m_coins;
    Wt::Dbo::ptr<Equipment> m_eq;

    void applyCoinChange(int amount, CoinType smaller, CoinType bigger);
};

}

DBO_EXTERN_TEMPLATES(Lancelot::Pouch)