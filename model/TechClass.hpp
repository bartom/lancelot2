#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include <Wt/Dbo/ptr.h>
#include "CommonEnums.hpp"

namespace Lancelot
{
class Character;
class Race;

class TechClass
{
public:
    TechClass();

    TechClass(TechClassEnum techClassId, ArmorClass armorClass, Parameter baseParam);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_id, "tech_class_enum"s);
        Wt::Dbo::field(a, m_weaponTypes, "weapon_types"s);
        Wt::Dbo::field(a, m_armorClass, "armor_class"s);
        Wt::Dbo::field(a, m_baseParam, "base_param"s);

        Wt::Dbo::hasMany(a, m_chars, Wt::Dbo::ManyToOne, "tech_class"s);
        Wt::Dbo::hasMany(a, m_races, Wt::Dbo::ManyToMany, "race_techclass"s);
    }

    void addWeaponType(WeaponType weaponType);
    auto getWeaponTypes() const;

    TechClassEnum asEnum() const
    { return m_id; }

    Parameter baseParam() const
    { return m_baseParam; }

private:
    TechClassEnum m_id;
    int m_weaponTypes;
    ArmorClass m_armorClass;
    Parameter m_baseParam;
    Wt::Dbo::collection<Wt::Dbo::ptr<Character>> m_chars;
    Wt::Dbo::collection<Wt::Dbo::ptr<Race>> m_races;

};

}

DBO_EXTERN_TEMPLATES(Lancelot::TechClass)