#pragma once

#include "ItemBase.hpp"
#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>

namespace Lancelot
{
class Equipment;

class Item : public ItemBase
{
public:
    Item();

    Item(std::string name, std::string description,
    int amount, double singularWeight);

    template<class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_name, "name"s);
        Wt::Dbo::field(a, m_description, "description"s);
        Wt::Dbo::field(a, m_amount, "amount"s);
        Wt::Dbo::field(a, m_singularWeight, "weight"s);
        Wt::Dbo::belongsTo(a, m_eq, "equipment"s);
    }

private:
    Wt::Dbo::ptr<Equipment> m_eq;
};

}

DBO_EXTERN_TEMPLATES(Lancelot::Item)
