#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include "ItemBase.hpp"

namespace Lancelot
{
class StatusChange;
class Equipment;

class Potion : public ItemBase
{
public:
    Potion();

    Potion(std::string name, std::string description,
    int amount, double singularWeight);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;
        Wt::Dbo::field(a, m_name, "name"s);
        Wt::Dbo::field(a, m_description, "description"s);
        Wt::Dbo::field(a, m_amount, "amount"s);
        Wt::Dbo::field(a, m_singularWeight, "weight"s);

        Wt::Dbo::hasOne(a, statusChange);
        Wt::Dbo::belongsTo(a, m_eq, "equipment"s);
    }

    Wt::Dbo::weak_ptr<StatusChange> statusChange;

private:
    Wt::Dbo::ptr<Equipment> m_eq;
};

}

DBO_EXTERN_TEMPLATES(Lancelot::Potion)