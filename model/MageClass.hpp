#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include "CommonEnums.hpp"
#include <Wt/Dbo/ptr.h>

namespace Lancelot
{
class Character;
class Race;

class MageClass
{
public:
    MageClass();

    MageClass(MageClassEnum mageClassId, Parameter baseParam);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_id, "mage_class_enum"s);
        Wt::Dbo::field(a, m_baseParam, "base_param"s);
        Wt::Dbo::field(a, m_deities, "deities"s);

        Wt::Dbo::hasMany(a, m_chars, Wt::Dbo::ManyToOne, "mage_class"s);
        Wt::Dbo::hasMany(a, m_races, Wt::Dbo::ManyToMany, "race_mageclass"s);
    }

    void addDeity(Deity deity);
    auto getDeities() const;

    MageClassEnum asEnum() const
    { return m_id; }

    Parameter baseParam() const
    { return m_baseParam; }

private:
    MageClassEnum m_id;
    Parameter m_baseParam;
    int m_deities;

    Wt::Dbo::collection<Wt::Dbo::ptr<Character>> m_chars;
    Wt::Dbo::collection<Wt::Dbo::ptr<Race>> m_races;

};

}

DBO_EXTERN_TEMPLATES(Lancelot::MageClass)