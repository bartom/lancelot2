#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>

namespace Lancelot
{
class User;
class Character;

using AuthInfo = Wt::Auth::Dbo::AuthInfo<User>;

enum class Role
{
    USER,
    ADMIN
};

class User
{
public:

    template<class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_role, "role"s);
        Wt::Dbo::hasMany(a, m_characters, Wt::Dbo::ManyToOne, "user"s);
    }

    void setRole(Role role) { m_role = role; }
    Role role() const { return m_role; }

    Role m_role;

    Wt::Dbo::collection<Wt::Dbo::ptr<Character>> m_characters;
    Wt::Dbo::weak_ptr<AuthInfo> m_authInfo;
};

}

DBO_EXTERN_TEMPLATES(Lancelot::User)
