#pragma once

#include <Wt/Dbo/Types.h>
#include <Wt/WGlobal.h>
#include "ItemBase.hpp"
#include "CommonEnums.hpp"

namespace Lancelot
{
class Equipment;
class StatusChange;

class Armor : public ItemBase
{
public:
    Armor();

    Armor(std::string name, std::string description, int amount,
          double singularWeight, ArmorType type,
          ArmorClass aclass, int damageReduction);

    template <class Action>
    void persist(Action& a)
    {
        using namespace std::string_literals;

        Wt::Dbo::field(a, m_name, "name"s);
        Wt::Dbo::field(a, m_description, "description"s);
        Wt::Dbo::field(a, m_amount, "amount"s);
        Wt::Dbo::field(a, m_singularWeight, "weight"s);

        Wt::Dbo::field(a, m_type, "type"s);
        Wt::Dbo::field(a, m_class, "aclass"s);
        Wt::Dbo::field(a, m_damageReduction, "damageReduc"s);
        Wt::Dbo::field(a, m_isEquipped, "equipped"s);

        Wt::Dbo::hasOne(a, statusChange);
        Wt::Dbo::belongsTo(a, m_eq, "equipment"s);
    }

    Wt::Dbo::weak_ptr<StatusChange> statusChange;

    ArmorType type() const
    { return m_type; }

    ArmorClass aClass() const
    { return m_class; }

    int damageReduction() const
    { return m_damageReduction; }

    bool isEquipped() const
    { return m_isEquipped; }

    void flipEquippedState();

private:
    ArmorType m_type;
    ArmorClass m_class;
    int m_damageReduction;
    bool m_isEquipped;

    Wt::Dbo::ptr<Equipment> m_eq;

};

}

DBO_EXTERN_TEMPLATES(Lancelot::Armor)